﻿using UnityEngine;
using System.Collections;
//using GameAnalyticsSDK;

public class GameAnalyticsController : BaseAnalyticsController
{
    // public GameAnalytics gameAnalyticsPrefab;

    // private void Awake()
    // {
    //     Instantiate(gameAnalyticsPrefab);
    //     GameAnalytics.Initialize();
    // }

    public override void LogLevelStart(int currentLevelNumHuman)
    {
        // Debug.Log("GA: Level start event; level = " + currentLevelNumHuman);
        // GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game", currentLevelNumHuman);
    }

    public override void LogLevelEnd(bool victory, int currentLevelNumHuman)
    {
        // if (victory)
        // {
        //     Debug.Log("GA: Level end event; level = " + currentLevelNumHuman);
        //     GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "game", currentLevelNumHuman);
        // }
    }
}
