﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class AnalyticsManager : MonoBehaviour, IAnalyticsController
{
    public BaseAnalyticsController[] analyticsControllers;

    public void LogLevelStart(int currentLevelNumHuman)
    {
        Debug.Log("Analytics: Level start event; level = " + currentLevelNumHuman);
        for (int i = 0; i < analyticsControllers.Length; i++)
        {
            var a = analyticsControllers[i];
            a.LogLevelStart(currentLevelNumHuman);
        }
    }

    public void LogLevelEnd(bool victory, int currentLevelNumHuman)
    {
        Debug.Log("Analytics: Level end event; level = " + currentLevelNumHuman);
        for (int i = 0; i < analyticsControllers.Length; i++)
        {
            var a = analyticsControllers[i];
            a.LogLevelEnd(victory, currentLevelNumHuman);
        }
    }
}

public interface IAnalyticsController
{
    void LogLevelEnd(bool victory, int currentLevelNumHuman);
    void LogLevelStart(int currentLevelNumHuman);
}

public abstract class BaseAnalyticsController : MonoBehaviour, IAnalyticsController
{
    public abstract void LogLevelStart(int currentLevelNumHuman);
    public abstract void LogLevelEnd(bool victory, int currentLevelNumHuman);
}
