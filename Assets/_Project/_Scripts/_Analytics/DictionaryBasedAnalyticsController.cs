﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class DictionaryBasedAnalyticsController : MonoBehaviour {

    protected abstract void LogAnalyticsEvent_Implementation(string eventName);
    protected abstract void LogAnalyticsEventWithParameters_Implementation(string eventName, Dictionary<string, string> parameters);
    public bool logAnalyticsEvent = true;

    public void LogAnalyticsEvent (string eventName) {
        if (logAnalyticsEvent && DebugHelper.isDebugBuild) {
            Debug.Log("ANALYTICS event = " + eventName);
        }

        LogAnalyticsEvent_Implementation(eventName);
    }

    public void LogAnalyticsEventWithParameters(string eventName, Dictionary<string, string> parameters)
    {
        if (logAnalyticsEvent && DebugHelper.isDebugBuild)
        {
			Debug.Log("ANALYTICS event = " + eventName + "; params : " + ToDebugString(parameters));
        }
        LogAnalyticsEventWithParameters_Implementation(eventName, parameters);
    }

    protected static string ToDebugString<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        return "{" + string.Join(",", dictionary.Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
    }
}
