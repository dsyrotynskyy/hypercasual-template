﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
//using Facebook.Unity;
using UnityEngine;

public class FacebookAnalyticsController : BaseAnalyticsController
{
    // void Awake()
    // {
    //     if (!FB.IsInitialized)
    //     {
    //         FB.Init(InitCallback, OnHideUnity);
    //     }
    //     else
    //     {
    //         FB.ActivateApp();
    //     }
    // }

    // private void InitCallback()
    // {
    //     if (FB.IsInitialized)
    //     {
    //         FB.ActivateApp();
    //     }
    //     else
    //     {
    //         Debug.Log("Failed to Initialize the Facebook SDK");
    //     }
    // }

    // private void OnHideUnity(bool isGameShown)
    // {
    //     if (!isGameShown)
    //     {
    //         // Pause the game - we will need to hide
    //         Time.timeScale = 0;
    //     }
    //     else
    //     {
    //         // Resume the game - we're getting focus again
    //         Time.timeScale = 1;
    //     }
    // }

    public override void LogLevelStart(int currentLevelNumHuman)
    {
        // Debug.Log("FB: Level start event; level = " + currentLevelNumHuman);

        // string fbKey = AnalyticsKeys.level_start;
        // Dictionary<string, object> fdict = new Dictionary<string, object>() { { AnalyticsKeys.level_num, currentLevelNumHuman } };
        // FB.LogAppEvent(fbKey, null, fdict);
    }

    public override void LogLevelEnd(bool victory, int currentLevelNumHuman)
    {
    }

    // public static class AnalyticsKeys
    // {
    //     public static string level_num = "level_num";
    //     public static string level_start = "level_start";
    //     public static string level_end = "level_end";
    // }
}
