﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SceneAnalytics : MonoBehaviour {

    [Inject]
    StageManager _stageManager;

    [Inject]
    AnalyticsManager _analyticsManager;

	void Start () {
        _stageManager.OnLevelEndEvent.AddListener(OnLevelEnd);
        _analyticsManager.LogLevelStart(_stageManager.CurrentLevelNumHuman);
    }

    private void OnDestroy()
    {
        _stageManager.OnLevelEndEvent.RemoveListener(OnLevelEnd);
    }

    private void OnLevelEnd(bool arg0)
    {
        if (arg0)
        {
            _analyticsManager.LogLevelEnd(true, _stageManager.CurrentLevelNumHuman);
        }
    }
}
