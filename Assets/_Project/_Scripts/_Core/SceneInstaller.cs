using UnityEngine;
using Zenject;

public class SceneInstaller : MonoInstaller
{
    public WorldColorManager WorldColorManager;
//    public NearMissManager NearMissManager;
//   public RoadManager RoadManager;
//    public CameraManager CameraManager;

    public override void InstallBindings()
    {
        Container.Bind<WorldColorManager>().FromInstance(WorldColorManager).AsSingle().Lazy();
//        Container.Bind<CameraManager>().FromInstance(CameraManager).AsSingle().Lazy();
//        Container.Bind<NearMissManager>().FromInstance(NearMissManager).AsSingle().Lazy();
//        Container.Bind<RoadManager>().FromInstance(RoadManager).AsSingle().Lazy();
        Container.Bind<SceneTimeManager>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
        Container.Bind<StageManager>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
    }
}