﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public abstract class BasePlayerAccount<T> : SingletonBehaviour<T> where T : MonoBehaviour
{
    protected DataStorage _dataStorage;

    [Inject]
    protected GameConfig _gameConfig;

    protected override void OnAwake()
    {
        base.OnAwake();
        _dataStorage = new DataStorage();
        Init();
    }

    protected abstract void Init();
    public abstract void Reset();

    protected override void OnSceneLoaded(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.LoadSceneMode arg1)
    {
        base.OnSceneLoaded(arg0, arg1);

        _dataStorage.SaveData();
    }

    protected override bool OrderDontDestroyOnLoad
    {
        get
        {
            return true;
        }
    }
}


public class StoredStringValue : StoredValue<string>
{
    public StoredStringValue(string id, DataStorage d, string defaultValue) : base(id, d, defaultValue)
    {
    }

    protected override string Read(string key, string defaultValue)
    {
        return _dataStorage.GetValue(key, defaultValue);
    }

    protected override void Store(string key, string newVal)
    {
        _dataStorage.SetValue(key, newVal);
    }
}

public class StoredFloatValue : StoredValue<float>
{
    public StoredFloatValue(string id, DataStorage d, float defaultValue) : base(id, d, defaultValue)
    {
    }

    protected override float Read(string key, float defaultValue)
    {
        return _dataStorage.GetValue(key, defaultValue);
    }

    protected override void Store(string key, float newVal)
    {
        _dataStorage.SetValue(key, newVal);
    }
}

public class HardcodedBoolValue
{
    bool _val;

    public HardcodedBoolValue(bool val)
    {
        _val = val;
    }

    public bool Value
    {
        get
        {
            return _val;
        }

        set
        {

        }
    }
}

public class StoredBoolValue : StoredValue<bool>
{
    public StoredBoolValue(string id, DataStorage d, bool defaultValue) : base(id, d, defaultValue)
    {
    }

    protected override bool Read(string key, bool defaultValue)
    {
        return _dataStorage.GetValue(key, defaultValue);
    }

    protected override void Store(string key, bool newVal)
    {
        _dataStorage.SetValue(key, newVal);
    }
}

public class StoredIntValue : StoredValue<int>
{
    public StoredIntValue(string id, DataStorage d, int defaultValue) : base(id, d, defaultValue)
    {
    }

    protected override int Read(string key, int defaultValue)
    {
        int res = _dataStorage.GetValue(key, defaultValue);
        return res;
    }

    protected override void Store(string key, int newVal)
    {
        _dataStorage.SetValue(key, newVal);
    }
}

public class StoredDateTimeValue : StoredValue<DateTime>
{
    public StoredDateTimeValue(string id, DataStorage d, DateTime defaultValue) : base(id, d, defaultValue)
    {
    }

    protected override DateTime Read(string key, DateTime defaultValue)
    {
        var s = _dataStorage.GetValue(key, defaultValue.ToString());
        DateTime res = DateTime.Parse(s);
        return res;
    }

    protected override void Store(string key, DateTime newVal)
    {
        _dataStorage.SetValue(key, newVal.ToString());
    }
}

public abstract class StoredValue<T>
{
    protected DataStorage _dataStorage;
    string _id;
    private T _defaultValue;

    public StoredValue(string id, DataStorage d, T defaultValue)
    {
        _id = id;
        _dataStorage = d;
        _defaultValue = defaultValue;
        _valueHashed = Read(_id, defaultValue);
    }

    internal UnityEvent OnValueChangedEvent = new UnityEvent();

    T _valueHashed;

    public T Value
    {
        get
        {
            return _valueHashed;
        }
        internal set
        {
            if (!System.Object.Equals(value, _valueHashed))
            {
                _valueHashed = value;
                Store(_id, _valueHashed);

                OnValueChangedEvent.Invoke();
            }
        }
    }

    public void Reset()
    {
        this.Value = _defaultValue;
    }

    protected abstract void Store(string key, T newVal);

    protected abstract T Read(string key, T defaultValue);
}

public class DataStorage
{
    public string GetValue(string key, string val)
    {
        return PlayerPrefs.GetString(key, val);
    }

    public void SetValue(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }

    public int GetValue(string key, int val)
    {
        return PlayerPrefs.GetInt(key, val);
    }

    internal float GetValue(string v1, float v2)
    {
        return PlayerPrefs.GetFloat(v1, v2);
    }

    internal long GetValue(string v1, long v2)
    {
        return (long)PlayerPrefs.GetFloat(v1, v2);
    }

    public void SetValue(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }

    public bool GetValue(string key, bool val)
    {
        int def;
        if (val)
        {
            def = 1;
        }
        else
        {
            def = 0;
        }
        return PlayerPrefs.GetInt(key, def) == 1;
    }

    public void SetValue(string key, bool state)
    {
        PlayerPrefs.SetInt(key, state ? 1 : 0);
    }

    internal void SetValue(string key, float val)
    {
        PlayerPrefs.SetFloat(key, val);
    }

    public void SaveData()
    {
        PlayerPrefs.Save();
    }
}

