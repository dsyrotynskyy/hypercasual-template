using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller
{
    public AnalyticsManager analyticsManager;
    public SoundStorage soundStorage;
    public LevelDataManager levelDataManager;
    public GameConfig gameConfig;
    public AudioPlayer audioPlayer;

    public override void InstallBindings()
    {
        Application.targetFrameRate = 60;
        DebugHelper.SetDebugBuild(gameConfig.DEBUG_Build);

        Container.Bind<AnalyticsManager>().FromComponentInNewPrefab(analyticsManager).AsSingle().NonLazy();
        Container.Bind<IInstantiator>().FromInstance(Container).AsSingle().NonLazy();
        Container.Bind<GameConfig>().FromInstance(gameConfig).AsSingle().NonLazy();
        Container.Bind<SoundStorage>().FromInstance(soundStorage).AsSingle().NonLazy();
        Container.Bind<LevelDataManager>().FromInstance(levelDataManager).AsSingle().NonLazy();

        Container.Bind<PlayerAccount>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
        Container.Bind<AudioPlayer>().FromComponentInNewPrefab(audioPlayer).AsSingle().NonLazy();

        Container.Bind<HapticFeedbackHelper>().FromNew().AsSingle();
    }
}