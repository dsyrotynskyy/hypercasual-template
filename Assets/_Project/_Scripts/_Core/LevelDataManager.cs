﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "_LevelDataManager", menuName = "Data/LevelDataManager")]
public class LevelDataManager : ScriptableObject
{
    [HideInInspector]
    public int[] targetScores = new int[] { 10, 20, 30, 40, 50 };

    [Header("Predefined")]
    public PredefinedLevel[] predefinedLevels;

    [Header("Pottery")]
    public TextAsset[] firstLevels;
    public TextAsset[] loopLevels;

    [Serializable]
    public class PredefinedLevel
    {
        public int targetLevel;
        public TextAsset textAsset;
        internal int TargetPoints;
    }

    [Header("Colors")]
    public LevelColorScheme[] firstColorSchemes;
    public LevelColorScheme[] loopColorSchemes;
    private System.Random _random;

    [Inject]
    GameConfig _gameConfig;

    bool _inited = false;

    public LevelInfo GetLevelInfo(int levelNum)
    {
        _random = new System.Random(levelNum);

        LevelInfo targetLevel;
        targetLevel = new LevelInfo();
        targetLevel.SetNum(levelNum);
        targetLevel.TargetFile = GetTextAsset(levelNum);
        targetLevel.TargetColorScheme = GetColorScheme(levelNum);

        return targetLevel;
    }

    TextAsset GetPredefinedLevel(int levelNum)
    {
        int levelNumHuman = levelNum + 1;
        for (int i = 0; i < this.predefinedLevels.Length; i++)
        {
            var level = predefinedLevels[i];
            if (level.targetLevel == levelNumHuman)
            {
                return level.textAsset;
            }
        }
        return null;
    }

    int GetPredefinedLevelPoints(int levelNum)
    {
        int levelNumHuman = levelNum + 1;
        for (int i = 0; i < this.predefinedLevels.Length; i++)
        {
            var level = predefinedLevels[i];
            if (level.targetLevel == levelNumHuman)
            {
                return level.TargetPoints;
            }
        }
        return -1;
    }

    TextAsset GetTextAsset(int i)
    {
        var predefinedLevel = GetPredefinedLevel(i);
        if (predefinedLevel != null)
        {
            return predefinedLevel;
        }

        if (i < firstLevels.Length)
        {
            return firstLevels[i];
        }

        i = i - firstLevels.Length;

        if (i >= loopLevels.Length)
        {
            i = i % loopLevels.Length;
        }

        var s = loopLevels[i];

        return s;
    }

    LevelColorScheme GetColorScheme(int i)
    {
        if (i < firstColorSchemes.Length)
        {
            return firstColorSchemes[i];
        }

        var allSchemes = loopColorSchemes;

        var s = allSchemes.GetRandomElement<LevelColorScheme>(_random);

        return s;
    }
}