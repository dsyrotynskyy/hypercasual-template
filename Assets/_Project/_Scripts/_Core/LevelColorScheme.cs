﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "_ColorScheme", menuName = "Data/ColorScheme")]
public class LevelColorScheme : ScriptableObject {
    public Color fogColor = new Color(0.75f, 0.95f , 0.95f);
    public Color cameraColor = new Color(0.35f, 0.05f, 0.05f);
    public Color roadColor = new Color(0.35f, 0.05f, 0.05f);
    public Color obstacleCarColor = new Color(0.35f, 0.05f, 0.05f);
    public Color playerCarColor = new Color(0.35f, 0.05f, 0.05f);
}
