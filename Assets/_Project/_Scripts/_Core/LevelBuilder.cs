﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelBuilder : MonoBehaviour
{
    [Inject]
    GameConfig GameConfig;
    [Inject]
    StageManager StageManager;
    public LevelDataManager LevelDataManager;
//    RoadManager roadManager;

    void Awake()
    {
        StageManager.FireOnGameLaunch(Init);
    }

    void Init () {
        var info = LevelDataManager.GetLevelInfo(StageManager.CurrentLevel);
//        this.roadManager = GetComponent<RoadManager>();
        var LevelData = Parse(info.TargetFile.text);
//        SetupLevel(LevelData);
//        roadManager.Init();
    }

    private LevelData Parse(string text)
    {
        LevelData level = new LevelData();

		level.hasInversedMainRoads = true;
		level.hasInversedSecondaryRoads = true;
        level.carSecondaryRoad = GameConfig.carsInSecondaryRoad;
        level.speed = GameConfig.carSpeed;
        level.seed = -1;

        string[] lines = text.Split('\n');
        foreach (var line in lines)
        {
            if (string.IsNullOrEmpty(line))
            {
                continue;
            }
            try
            {
                string[] elements = line.Split(':');
                if (elements[0] == "carSecondaryRoad") {
                    int carSecondaryRoad = int.Parse(elements[1]);
                    level.carSecondaryRoad = carSecondaryRoad;
                } else
                if (elements[0] == "cs") {
                    float speed = float.Parse(elements[1]);
                    level.speed = speed;
                }
                else if (elements[0] == "im")
                {
                    bool reverse = bool.Parse(elements[1]);
                    level.hasInversedMainRoads = reverse;
                }
				    else if (elements[0] == "is")
                {
                    bool reverse = bool.Parse(elements[1]);
                    level.hasInversedSecondaryRoads = reverse;
                }
                else
                if (elements[0] == "m")
                {
                    int main = int.Parse(elements[1]);
                    level.mainRoadsNum = main;
                }
                else if (elements[0] == "s")
                {
                    int sec = int.Parse(elements[1]);
                    level.secondaryRoadsNum = sec;
                }
                else if (elements[0] == "seed")
                {
                    int seed = int.Parse(elements[1]);
                    level.seed = seed;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }
        }

        return level;
    }

    // void SetupLevel(LevelData LevelData)
    // {
    //     roadManager.hasInversedMainRoads = LevelData.hasInversedMainRoads;
	// 	roadManager.hasInversedSecondaryRoads = LevelData.hasInversedSecondaryRoads;
    //     roadManager.mainRoadsNum = LevelData.mainRoadsNum;
    //     roadManager.secondaryRoadsNum = LevelData.secondaryRoadsNum;
    //     roadManager.carSpeed = LevelData.speed;
    //     roadManager.carSecondaryRoad = LevelData.carSecondaryRoad;
    //     roadManager.targetSeed = LevelData.seed;
    // }

    public struct LevelData
    {
        public int mainRoadsNum;
        public int secondaryRoadsNum;
        public bool hasInversedMainRoads;
        public bool hasInversedSecondaryRoads;
        public float speed;
        internal int carSecondaryRoad;
        internal int seed;
    }
}
