﻿using System;
using UnityEngine;
using Zenject;

public class WorldColorManager : SingletonBehaviour<WorldColorManager>
{
    [Inject]
    LevelDataManager LevelDataManager;
    [Inject]
    StageManager StageManager;

    public FireEvent initEvent = new FireEvent();

    [SerializeField]
    private Material roadMaterialTemplate;
    [HideInInspector]
    public Material roadMaterial;
    [SerializeField]
    private Material obstacleMaterialTemplate;
    [HideInInspector]
    public Material obstacleMaterial;
    [SerializeField]
    private Material playerMaterialTemplate;
    [HideInInspector]
    public Material playerMaterial;

    LevelColorScheme LevelColorScheme;

    void Start()
    {
        StageManager.FireOnGameLaunch(Init);

        LevelColorScheme = StageManager.LevelInfo.TargetColorScheme;

        RenderSettings.fogColor = LevelColorScheme.fogColor;

        // And enable fog
        // RenderSettings.fog = true;

        // roadMaterial = CreateMaterial(roadMaterialTemplate, LevelColorScheme.roadColor);
        // obstacleMaterial = CreateMaterial(obstacleMaterialTemplate, LevelColorScheme.obstacleCarColor);
        // playerMaterial = CreateMaterial(playerMaterialTemplate, LevelColorScheme.playerCarColor);

        initEvent.Invoke();
    }

    Material CreateMaterial(Material template, Color color)
    {
        var mat = new Material(template);
        mat.SetColor("_Color", color);
        return mat;
    }
    private void Init()
    {
        initEvent.Invoke();
    }
}
