﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfo
{
    public void SetNum(int num)
    {
        NumHuman = num + 1;
    }
    public int NumHuman { get; private set; }
    public TextAsset TargetFile;

    public LevelColorScheme TargetColorScheme { get; internal set; }
}
