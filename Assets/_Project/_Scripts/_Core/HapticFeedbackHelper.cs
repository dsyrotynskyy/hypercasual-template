﻿using System;
using UnityEngine;
using Zenject;

public class HapticFeedbackHelper {

    [Inject]
    PlayerAccount _playerAccount;

    public void DoHeavy () {
        if (!_playerAccount.IsVibroOn.Value)
        {
            return;
        }
        if (Application.isEditor)
        {
            return;
        }

        try {
#if UNITY_ANDROID && !UNITY_EDITOR
        AnroidHapticFeedback (HapticFeedback.HapticForce.Medium);
        return;
#endif

            HapticFeedback.DoHaptic(HapticFeedback.HapticForce.Heavy);
        } catch (Exception e) {
            Debug.Log(e.ToString ());
        }
    }

    public void DoMedium()
    {
        if (!_playerAccount.IsVibroOn.Value)
        {
            return;
        }

        if (Application.isEditor)
        {
            return;
        }

        try
        {
#if UNITY_ANDROID && !UNITY_EDITOR
        AnroidHapticFeedback (HapticFeedback.HapticForce.Medium);
        return;
#endif

            HapticFeedback.DoHaptic(HapticFeedback.HapticForce.Medium);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public void DoLight()
    {
        if (!_playerAccount.IsVibroOn.Value)
        {
            return;
        }

        if (Application.isEditor)
        {
            return;
        }

        try
        {
#if UNITY_ANDROID && !UNITY_EDITOR
        AnroidHapticFeedback (HapticFeedback.HapticForce.Light);
        return;
#endif

            HapticFeedback.DoHaptic(HapticFeedback.HapticForce.Light);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

        private class HapticFeedbackManager
    {
        private int HapticFeedbackConstantsKey;
        private int HapticFeedbackMediumKey;
#if UNITY_ANDROID && !UNITY_EDITOR

        private AndroidJavaObject UnityPlayer;
#endif

        public HapticFeedbackManager()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            Debug.Log("Android haptic");
            HapticFeedbackConstantsKey=new AndroidJavaClass("android.view.HapticFeedbackConstants").GetStatic<int>("VIRTUAL_KEY");
//            HapticFeedbackMediumKey=new AndroidJavaClass("android.view.HapticFeedbackConstants").GetStatic<int>("VIRTUAL_KEY_RELEASE");
            UnityPlayer=new AndroidJavaClass ("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer");
            //Alternative way to get the UnityPlayer:
            //int content=new AndroidJavaClass("android.R$id").GetStatic<int>("content");
            //new AndroidJavaClass ("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("findViewById",content).Call<AndroidJavaObject>("getChildAt",0);
#endif
        }

        public bool Execute(HapticFeedback.HapticForce force)
        {
            int power;

            if (force == HapticFeedback.HapticForce.Light)
            {
                power = HapticFeedbackConstantsKey;
            }
            else
            {
                power = HapticFeedbackMediumKey;
            }

#if UNITY_ANDROID && !UNITY_EDITOR
            return UnityPlayer.Call<bool> ("performHapticFeedback",power);
#endif
            return false;
        }
    }

    //Cache the Manager for performance
    private static HapticFeedbackManager mHapticFeedbackManager;

    static bool AnroidHapticFeedback(HapticFeedback.HapticForce force)
    {
        if (mHapticFeedbackManager == null)
        {
            mHapticFeedbackManager = new HapticFeedbackManager();
        }
        return mHapticFeedbackManager.Execute(force);
    }
}
