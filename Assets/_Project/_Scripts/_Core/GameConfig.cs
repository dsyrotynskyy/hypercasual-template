using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "_GameConfig", menuName = "Data/GameConfig")]
public class GameConfig : ScriptableObject
{
    [Header("Debug")]
    public bool DEBUG_Build;
    public bool GOD_MODE = false;

    public bool PSEUDORANDOM;

    [HideInInspector]
    public int ReviewRequestFrequency = 5;
    public float carSpeed = 2f;
    public int carsInSecondaryRoad = 5;
    public float secondaryRoadAngle = 30f;
}