﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class StageManager : MonoBehaviour
{
    [Inject]
    PlayerAccount _playerAccount;

    bool _isGameEnd;

    [HideInInspector]
    public EndGameEvent OnLevelEndEvent = new EndGameEvent();

    public bool IsGameEnded { get { return _isGameEnd; } }

    internal void WinGame()
    {
        EndGame(true);
    }

    public int Score { get; private set; }

    public LevelInfo LevelInfo{
        get {
            CheckLevel();
            return _levelInfo;
        }
    }

    internal void FailGame()
    {
        EndGame(false);
    }

    private LevelInfo _levelInfo;

    [Inject]
    private GameConfig _gameConfig;

    [Inject]
    private LevelDataManager _levelDataManager;

    public bool IsGameLaunched { get; private set; }

    protected void Awake()
    {  
        CheckLevel();

        IsGameLaunched = true;
        _initEvent.Invoke();
    }

    FireEvent _initEvent = new FireEvent();

    public void FireOnGameLaunch (Action action) {
        _initEvent.AddListener(action);
    }

    public int CurrentLevelNumHuman { get { return CurrentLevel + 1; } }
    public int CurrentLevel { get; private set; }

    void CheckLevel () {
        if (_levelInfo != null) {
            return;
        }

        CurrentLevel = _playerAccount.CurrentLevel.Value;

        _levelInfo = _levelDataManager.GetLevelInfo(CurrentLevel);
    }

    void EndGame (bool victory) {
        if (_isGameEnd) {
//            Debug.LogError("WTF!!!");
            return;
        }
        _isGameEnd = true;

        if (victory)
        {
            IsGameWin = true;
        }
        else
        {
            IsGameFailure = true;
        }

        OnLevelEndEvent.Invoke(victory);

        if (victory) {
            int currentScore = _playerAccount.CurrentLevel.Value + 1;

            _playerAccount.CurrentLevel.Value++;

            try
            {
                if (_playerAccount.CurrentLevel.Value % _gameConfig.ReviewRequestFrequency == 0)
                {
                    Debug.Log("Request review");
#if UNITY_IOS
//                iOSNativeReviewRequest.RequestReview();
                Device.RequestStoreReview();
#endif
                }
            } catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }

            IsGameWin = true;
        }
    }

    public bool IsTutorialLevel {
        get {
            bool isTutorial = this.LevelInfo.NumHuman == 1;
            return isTutorial;
        }
    }

    public bool IsGameWin { get; internal set; }
    public bool IsGameFailure { get; private set; }

    [System.Serializable]
    public class EndGameEvent : UnityEvent<bool>
    {
    }
}
