﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CameraVictory : MonoBehaviour {

	[Inject]
	StageManager StageManager;

	public GameObject particles;

	void Start () {
		particles.SetActive(false);
		StageManager.OnLevelEndEvent.AddListener(OnLevelEnd);
	}

    private void OnLevelEnd(bool a)
    {
		if (a) {
        	particles.SetActive(true);
		}
    }
}
