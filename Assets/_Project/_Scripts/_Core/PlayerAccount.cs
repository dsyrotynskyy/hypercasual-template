﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class PlayerAccount : BasePlayerAccount<PlayerAccount> {

    [Inject]
    AudioPlayer audioPlayer;

    public StoredBoolValue RemovedAds { get; private set; }
    public StoredIntValue CurrentLevel { get; private set; }
    public StoredIntValue ActualGamePoints { get; private set; }
    public StoredIntValue BestGamePoints { get; private set; }

    public StoredBoolValue IsSfxOn { get; private set; }
    public StoredBoolValue IsVibroOn { get; private set; }
    public StoredBoolValue IsDarkModeOn { get; private set; }

    protected override void Init()
    {
        CurrentLevel = new StoredIntValue("CurrentLevel", _dataStorage, 0);
        ActualGamePoints = new StoredIntValue("TotalScoreLevel", _dataStorage, 0);
        BestGamePoints = new StoredIntValue("BestGamePoints", _dataStorage, 0);
        RemovedAds = new StoredBoolValue("RemovedAds", _dataStorage, false);

        IsSfxOn = new StoredBoolValue("IsSfxOn", _dataStorage, true);
        IsVibroOn = new StoredBoolValue("IsVibroOn", _dataStorage, true);
        IsDarkModeOn = new StoredBoolValue("IsDarkModeOn", _dataStorage, false);

        PostInit();
    }

    internal void OrderRemoveAds()
    {
        RemovedAds.Value = true;
    }

    void PostInit () {
        IsSfxOn.OnValueChangedEvent.AddListener(() => {
            this.Invoke("UpdateSound", 0f);
        });
        this.Invoke("UpdateSound", 0f);
    }

    void UpdateSound ()
    {
        audioPlayer.SetVolume(IsSfxOn.Value);
    }

    public override void Reset () {
        CurrentLevel.Reset();
        ActualGamePoints.Reset();
        BestGamePoints.Reset();
        RemovedAds.Reset();
        IsSfxOn.Reset();
        IsVibroOn.Reset();
        IsDarkModeOn.Reset();
    }

    public int CurrentLevelNumHuman {
        get {
            return this.CurrentLevel.Value + 1;
        }
    }
}
