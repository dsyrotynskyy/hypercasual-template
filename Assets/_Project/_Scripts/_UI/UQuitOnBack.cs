﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UQuitOnBack : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Quit App");
            Application.Quit();
        }
    }
}
