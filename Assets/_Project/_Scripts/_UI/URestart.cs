﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class URestart : MonoBehaviour {
   public float displayDelay = 1f;
   public GameObject restartButtonRoot;
//   public Button restartButton;
   [Inject]
   StageManager StageManager;
//    public Text bestScoreText;
//    public string failText = "Restart";

   bool _restartEnabled = false;

	void Start () {

       StageManager.OnLevelEndEvent.AddListener(OnGameEndedEvent);

//       restartButton.onClick.AddListener(OnRestartSceneButtonClick);
	}

   private void OnGameEndedEvent(bool victory)
   {
       if (victory) {
           gameObject.SetActive(false);
       } else {
           this.Invoke(displayDelay, DisplayRestartUI);
       }
   }

   void DisplayRestartUI () {
       restartButtonRoot.gameObject.SetActive(true);

       _restartEnabled = true;
   }

   private void OnRestartSceneButtonClick()
   {
       SceneManager.LoadScene("_GameScene");
   }

   void Update()
   {
       if (!_restartEnabled) {
           return;
       }

       if (Input.GetKeyDown(KeyCode.Return))
       {
           OnRestartSceneButtonClick();
       }

       if (Input.GetMouseButtonDown(0))
       {
           OnRestartSceneButtonClick();
       }
   }
}
