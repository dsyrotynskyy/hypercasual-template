﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class UNextLevel : MonoBehaviour
{
    [Inject]
    public AudioPlayer _audioPlayer;
    public AudioClip victorySound;

    public float GoToNextLevelDelay = 0.3f;
    public float DisplayDelay = 0.5f;
    public Text text;
    public GameObject restartButtonRoot;
    public Button restartButton;

    [Inject]
    StageManager _stageManager;
    public string victoryText = "Level {0} completed!";
    bool _restartEnabled = false;

    void Start()
    {
        _stageManager.OnLevelEndEvent.AddListener(OnGameEndedEvent);

        restartButton.onClick.AddListener(OnRestartSceneButtonClick);
    }

    private void OnGameEndedEvent(bool victory)
    {
        if (victory)
        {
            foreach (var g in deactivateOnVictory)
            {
                if (g)
                {
                    g.SetActive(false);
                }
            }

            this.Invoke(DisplayDelay, DisplayVictory);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public GameObject[] deactivateOnVictory;

    void DisplayVictory () {
        if (victorySound)
        {
            _audioPlayer.Play(victorySound);
        }

        _restartEnabled = true;
        restartButtonRoot.gameObject.SetActive(true);
        restartButton.targetGraphic.gameObject.SetActive(true);

        string t = string.Format(victoryText, _stageManager.LevelInfo.NumHuman);
        text.text = t;

        this.Invoke(GoToNextLevelDelay, EnableGoToNextLevel);
    }

    void EnableGoToNextLevel ()
    {
        _restartEnabled = true;
    }

    private void OnRestartSceneButtonClick()
    {
        SceneManager.LoadScene("_GameScene");
    }

    void Update()
    {
        if (!_restartEnabled)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            OnRestartSceneButtonClick();
        }
    }
}
