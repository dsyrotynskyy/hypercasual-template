﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FlashCanvas : MonoBehaviour {

    public float StartAlpha = 0.5f;
    public Ease easeType = Ease.InCirc;
    public float fadeTime = 1f;
    public CanvasGroup alpha;
//    ArrowManager _arrowManager;

	void Start () {
        //_arrowManager = ArrowManager.GetInstance;
        //_arrowManager.FeverLaunchEvent.AddListener(OnFeverFire);
	}
	
    void OnFeverFire () {
        alpha.alpha = StartAlpha;
        DOTween.To(() => alpha.alpha, x => alpha.alpha = x, 0f, fadeTime).SetEase(easeType);
    }
}
