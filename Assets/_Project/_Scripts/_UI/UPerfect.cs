﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UPerfect : MonoBehaviour {

    [Inject]
    AudioPlayer audioPlayer;

    public Animator _animator;
    public Text messageText;
    public string[] Messages = new string[]
    {
            "COOL!",
            "GREAT!",
            "WOW!",
            "NICE!",
            "AMAZING!",
            "FANTASTIC!"
    };

    public string OutOfRangeMessage = "OMG!";

    public AudioClip comboPerformedSound;

	void Start () {
//        ComboManager.GetInstance.arrowComboEvent.AddListener(OnArrowEvent);

//        PerfectHitManager b = PerfectHitManager.GetInstance;
	}

    private void OnArrowEvent(int arrowNum)
    {
        OnPerfect(arrowNum);
    }

    private void OnPerfect(int arrowCount)
    {
        int minArrowCount = 3;
        if (arrowCount < minArrowCount) {
            return;
        }

        _animator.enabled = true;
        _animator.Play("Perfect");

        string str;
        int targetElement = arrowCount - minArrowCount;
        if (targetElement >= Messages.Length) {
            targetElement = Messages.Length - 1;
            int x = arrowCount + 1;
            str = "x" + x + " " + OutOfRangeMessage;
        } else {
            str = Messages[targetElement];
        }

        messageText.text = str;

        if (comboPerformedSound) {
            audioPlayer.Play(comboPerformedSound);
        }
    }
}
