﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UNearMiss : MonoBehaviour {

	// [Inject]
	// NearMissManager NearMissManager;

	public Animator nearMiss;
	void Start () {
//		NearMissManager.onNearMissEvent.AddListener(OnNearMiss);
	}

    private void OnNearMiss()
    {
		nearMiss.enabled = true;
    }

	void OnNearMissAnimationEnded () {
		nearMiss.enabled = false;
	}
}
