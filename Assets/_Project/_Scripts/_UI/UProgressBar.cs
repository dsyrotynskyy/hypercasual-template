﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UProgressBar : MonoBehaviour {

	// [Inject]
	// RoadManager RoadManager;
	[Inject]
	StageManager StageManager;
	public Image foregroundImage;

	public Text textCurrent;
	public Text textNext;

	void Start () {
		StageManager.FireOnGameLaunch(Init);
//		RoadManager.onCarUnregisteredEvent.AddListener(UpdateProgress);
		UpdateProgress();
	}

    // private void UpdateProgress(PlayerCar arg0)
    // {
	// 	// int totalCars = RoadManager.carSecondaryRoad * RoadManager.secondaryRoadsNum;
    //     // int num = RoadManager.UnregisteredCarNum;
	// 	float p = ((float)num / totalCars);
	// 	foregroundImage.fillAmount = p;
    // }

    void Init () {
		textCurrent.text = StageManager.CurrentLevelNumHuman + "";
		textNext.text = (StageManager.CurrentLevelNumHuman + 1) + "";
	}

	void UpdateProgress () {

	}
}
