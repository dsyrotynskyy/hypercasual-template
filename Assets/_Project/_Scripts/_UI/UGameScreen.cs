﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UGameScreen : MonoBehaviour {

    //public float skipButtonActivationDelay = 25f;
    //public Button skipButton;
    //public GameObject root;
    //bool _canUseSkipButton = true;
    //PsdkEventSystem psdkEventSystem;
    //bool _rvImpressionSent = false;

    //void Start()
    //{
    //    DisableSkipButton();
    //    if (PSDKMgr.Instance.GetRewardedAdsService() != null && PSDKMgr.Instance.GetRewardedAdsService().IsAdReady())
    //    {
    //        ActivateSkipButton();
    //    }
    //    psdkEventSystem = PsdkEventSystem.Instance;

    //    psdkEventSystem.onRewardedAdIsReadyEvent += ActivateSkipButton;
    //    psdkEventSystem.onRewardedAdIsNotReadyEvent += DisableSkipButton;
    //    psdkEventSystem.onRewardedAdDidClosedWithResultEvent += OnRewardedAdClosed;

    //    if (Application.isEditor)
    //    {
    //        ActivateSkipButton();
    //    }
    //    skipButton.onClick.AddListener(OnSkipButtonClick);
    //    UnityEngine.XR.WSA.WorldManager.GetInstance.OnLevelEndEvent.AddListener(OnGameWin);
    //}

    //private void OnDestroy()
    //{
    //    psdkEventSystem.onRewardedAdIsReadyEvent -= ActivateSkipButton;
    //    psdkEventSystem.onRewardedAdIsNotReadyEvent -= DisableSkipButton;
    //    psdkEventSystem.onRewardedAdDidClosedWithResultEvent -= OnRewardedAdClosed;
    //}

    //private void OnGameWin(bool arg0)
    //{
    //    _canUseSkipButton = false;
    //    DisableSkipButton();
    //}

    //private void OnSkipButtonClick()
    //{
    //    if (PSDKMgr.Instance.GetRewardedAdsService() != null && PSDKMgr.Instance.GetRewardedAdsService().IsAdReady())
    //    {
    //        if (PSDKMgr.Instance.GetRewardedAdsService().ShowAd())
    //        {
    //            DisableSkipButton();
    //        }
    //        else
    //        {
    //            Debug.LogError("UGameScreen:: OnSkipButtonClick: clicked but failed to show");
    //        }
    //    }
    //    else
    //    {
    //        Debug.LogError("UGameScreen:: OnSkipButtonClick: clicked but ad was not ready");
    //    }

    //    if (Application.isEditor)
    //    {
    //        OnRewardedAdClosed(true);
    //    }
    //}

    //private void OnRewardedAdClosed(bool shouldReward)
    //{
    //    Debug.Log("UGameScreen:: OnRewardedAdClosed: " + shouldReward);
    //    if (shouldReward)
    //    {
    //        UnityEngine.XR.WSA.WorldManager.GetInstance.WinGame();
    //        _canUseSkipButton = false;
    //    }
    //    else
    //    {
    //        ActivateSkipButton();
    //    }
    //}

    //void ActivateSkipButton()
    //{
    //    if (_canUseSkipButton)
    //    {
    //        skipButton.gameObject.SetActive(true);
    //        if (!_rvImpressionSent)
    //        {
    //            if (PSDKMgr.Instance.GetAnalyticsService() != null)
    //            {
    //                Dictionary<string, object> eventParams = new Dictionary<string, object>
    //                {
    //                    {"location", "run"},
    //                    {"rvAvailable", 1},
    //                    {"rvType","skip"}
    //                };
    //                PSDKMgr.Instance.GetAnalyticsService().LogEvent(AnalyticsTargets.ANALYTICS_TARGET_DELTA_DNA, "rvImpression", eventParams, false);
    //            }
    //        }
    //        _rvImpressionSent = true;
    //    }
    //}

    //void DisableSkipButton()
    //{
    //    skipButton.gameObject.SetActive(false);
    //    _rvImpressionSent = false;
    //}
}
