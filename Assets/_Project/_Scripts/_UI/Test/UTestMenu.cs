﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class UTestMenu : MonoBehaviour {

    [Inject]
    GameConfig _gameConfig;

    public string gameSceneName = "_GameScene";
    public string vibroTestSceneName = "_VibroTestScene";
    public string tapticNewTestSceneName = "Sample UI";

    public Button startGameButton;
    public Button buttonResetAll; 
    public Button testVibroButton;
    public Button testTapticNew;

    public InputField targetLevelInputField;
    public InputField sensetivityModifierInputField;
    public InputField accuracyModifierInputField;
    public InputField accuracyOutModifierInputField;
    public Toggle paintTileAnimation;

    [Inject]
    PlayerAccount _playerAccount;

    void Start () {
        buttonResetAll.onClick.AddListener(ResetProgress);
        startGameButton.onClick.AddListener(OnStartButtonClick);
        testVibroButton.onClick.AddListener(TestVibro);
        testTapticNew.onClick.AddListener(TestTapticNew);

        UpdateUI();
	}

    private void TestTapticNew()
    {
        SceneManager.LoadScene(tapticNewTestSceneName);
    }

    void UpdateUI () {
        int level = _playerAccount.CurrentLevel.Value + 1;
        targetLevelInputField.text = level.ToString();

//        sensetivityModifierInputField.text = _gameConfig.DEBUG_Sensetivity.ToString();
        // accuracyModifierInputField.text = _gameConfig.Victory_LayerDeltaAccuracy.ToString();
        // accuracyOutModifierInputField.text = _gameConfig.Victory_LayerDeltaAccuracyOut.ToString();
    }

    private void ResetProgress()
    {
        _playerAccount.Reset();
        UpdateUI();
    }

    private void OnStartButtonClick()
    {
        SceneManager.LoadScene(gameSceneName);

        int level = int.Parse(targetLevelInputField.text) - 1;
        _playerAccount.CurrentLevel.Value = level;

//        _gameConfig.DEBUG_Sensetivity = float.Parse(sensetivityModifierInputField.text);
        // _gameConfig.Victory_LayerDeltaAccuracy = float.Parse(accuracyModifierInputField.text);
        // _gameConfig.Victory_LayerDeltaAccuracyOut = float.Parse(accuracyOutModifierInputField.text);
    }

    private void TestVibro()
    {
        SceneManager.LoadScene(vibroTestSceneName);
    }
}
