﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UDebugNextLevel : MonoBehaviour {

    public Button button;

    [Inject]
    StageManager _stageManager;

    void Start()
    {
        if (Debug.isDebugBuild)
        {
            button.onClick.AddListener(OnButtonClick);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void OnButtonClick()
    {
        _stageManager.WinGame();
    }
}
