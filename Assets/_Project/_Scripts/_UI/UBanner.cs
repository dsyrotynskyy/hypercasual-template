﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UBanner : MonoBehaviour {

	public GameObject[] banners;
	// Use this for initialization
	void Start () {
		var lucky = banners.GetRandomElement<GameObject> ();
		foreach (var b in banners) {
			b.SetActive(b == lucky);
		}
	}
	

}
