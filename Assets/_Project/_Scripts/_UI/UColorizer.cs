﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UColorizer : MonoBehaviour {

    Graphic _graphic;
    LevelColorScheme _levelColorScheme;
    UnityEngine.XR.WSA.WorldManager _worldManager;
    [Inject]
    WorldColorManager _worldColorManager;
    [Inject]
    PlayerAccount _playerAccount;

    void Start()
    {
        _graphic = GetComponent<Graphic>();
        //_worldManager = WorldManager.GetInstance;
        //_worldManager.FireOnGameLaunch(Init);
    }

    private void OnDestroy()
    {
        if (_playerAccount)
        {
            _playerAccount.IsDarkModeOn.OnValueChangedEvent.RemoveListener(UpdateColors);
        }
    }

    private void Init()
    {
        _playerAccount.IsDarkModeOn.OnValueChangedEvent.AddListener(UpdateColors);
        UpdateColors();
    }

    void UpdateColors()
    {
        // bool isDarkColor = true;

        // isDarkColor = _playerAccount.IsDarkModeOn.Value;

        // Color targetColor = _worldColorManager.uiColorLight;

        // if (isDarkColor)
        // {
        //     targetColor = _worldColorManager.uiColorDark;
        // }

//        _graphic.color = targetColor;
    }
}
