﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UTutorial : MonoBehaviour {

    // [Inject]
    // PotteryInput _potteryInput;

    [Inject]
    StageManager _stageManager;

    // [Inject]
    // PotteryMarkersManager _potteryMarkersManager;

    public GameObject tutorialRoot;

    // public Animator animatorUp;
    // public Animator animatorRight;
    // public Animator animatorInside;

    void Start()
    {
        if (_stageManager.IsTutorialLevel) {
            tutorialRoot.gameObject.SetActive(true);
        } else {
            tutorialRoot.gameObject.SetActive(false);
        }
    }

    private void Update() {
        if (Input.GetMouseButtonDown (0)) {
            tutorialRoot.gameObject.SetActive(false);
        }
    }
}
