﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UScore : MonoBehaviour {
    public Text levelCurrent;
    public Text levelNext;
    public Image progressImage;
    public Image[] imagesToColor;

    [Inject]
    StageManager _stageManager;

	void Start () {
 //       _stageManager.scoreChangedEvent.AddListener(DisplayScore);

        int levelNumHuman = _stageManager.CurrentLevel + 1;
        int levelNextHuman = levelNumHuman + 1;

        levelCurrent.text = levelNumHuman.ToString();
        levelNext.text = (levelNextHuman).ToString();

        DisplayScore();

        var l = _stageManager.LevelInfo;
        var scheme = l.TargetColorScheme;
        //var color = scheme.circleColors[0];

        //foreach (var i in imagesToColor)
        //{
        //    i.color = color;
        //}
    }

    private void DisplayScore()
    {
        //float score = _worldManager.Score;
        //float targetScore = _worldManager.TargetScore;

        //float percent = score / targetScore;
        //this.progressImage.fillAmount = percent;

        //string s = score + "/" + targetScore;
        //scoreText.text = s;

        //if (score > 0) {
        //    animator.SetTrigger("score-changed");
        //}
    }
}
