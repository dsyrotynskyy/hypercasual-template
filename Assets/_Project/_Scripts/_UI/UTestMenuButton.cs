﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UTestMenuButton : MonoBehaviour {
    public string testMenuSceneName = "_TestMenuScene";
    public Button button;

	void Start () {
        button.onClick.AddListener(OnButtonClick);
	}

    private void OnButtonClick()
    {
        SceneManager.LoadScene("_TestMenuScene");
    }
}
