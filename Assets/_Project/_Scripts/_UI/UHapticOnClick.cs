﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UHapticOnClick : MonoBehaviour {

    [Inject]
    HapticFeedbackHelper hapticFeedbackHelper;

    protected Button _button;

    protected virtual void Awake()
    {
        _button = GetComponent<Button>();

        if (!_button)
        {
            Debug.LogError("Missing Button script on " + name);
            return;
        }

        _button.onClick.AddListener(OnClickAction);
    }

    private void OnClickAction()
    {
        hapticFeedbackHelper.DoLight();
    }
}
