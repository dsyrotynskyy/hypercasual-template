﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UCurrentLevel : MonoBehaviour {

    public Text scoreText;
    [Inject]
    StageManager _worldManager;

    void Start()
    {
        DisplayScore();
    }

    private void DisplayScore()
    {
        string s = "Level " + _worldManager.LevelInfo.NumHuman;
        scoreText.text = s;
    }
}
