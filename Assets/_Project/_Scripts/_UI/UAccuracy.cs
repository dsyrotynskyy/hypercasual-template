﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UAccuracy : MonoBehaviour {

    public float LerpSpeed = 5f;

    // [Inject]
    // PotteryChecker potteryChecker;

    public Image progressImage;
    public Text progressText;

    float _targetProgress = -1f;
    float _actualProgress = 0f;

	void Start () {
        CalculateProgress();
    }
	
	void FixedUpdate () {
        CalculateProgress();
    }

    void CalculateProgress()
    {
//        _targetProgress = potteryChecker.AccuracyProgress;
        if (_targetProgress >= 1f)
        {
            UpdateUI(1f);
        }
    }

    int prevActualProgressInt = -1;

    private void Update()
    {
        UpdateUI(Time.deltaTime);
    }

    void UpdateUI (float deltaTime)
    {
        _actualProgress = Mathf.Lerp(_actualProgress, _targetProgress, deltaTime * LerpSpeed);

        var actualProgressInt = (int)(_actualProgress * 100);
        if (actualProgressInt != prevActualProgressInt)
        {
            prevActualProgressInt = actualProgressInt;
            progressText.text = actualProgressInt + "%";
        }

        progressImage.fillAmount = _actualProgress;
    }
}
