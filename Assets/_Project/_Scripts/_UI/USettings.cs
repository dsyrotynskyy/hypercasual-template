﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class USettings : MonoBehaviour
{

    public UToggleButton settingsButton;
    public GameObject SettingsPopup;
    public UToggleButton sfxButton;
    public UToggleButton vibroButton;
    public UToggleButton darkModeButton;

    [Inject]
    PlayerAccount _playerAccount;

    void Start()
    {
        settingsButton.onValueChangedEvent.AddListener(UpdateUI);

        sfxButton.IsOn = _playerAccount.IsSfxOn.Value;
        sfxButton.onValueChangedEvent.AddListener(UpdateUI);
        vibroButton.IsOn = _playerAccount.IsVibroOn.Value;
        vibroButton.onValueChangedEvent.AddListener(UpdateUI);
        darkModeButton.IsOn = _playerAccount.IsDarkModeOn.Value;
        darkModeButton.onValueChangedEvent.AddListener(UpdateUI);

        UpdateUI(false);

        //PlayerController player;
        //player = Player.GetInstance.GetComponent<PlayerController>();
        //player.onPlayerMovementStartedEvent.AddListener(OnMovementStarted);
        gameObject.SetActive(true);
    }

    //private void OnMovementStarted(Tile arg0, Tile arg1)
    //{
    //    settingsButton.IsOn = false;
    //}

    private void UpdateUI(bool arg0)
    {
        SettingsPopup.SetActive(settingsButton.IsOn);

        _playerAccount.IsSfxOn.Value = sfxButton.IsOn;
        _playerAccount.IsVibroOn.Value = vibroButton.IsOn;
        _playerAccount.IsDarkModeOn.Value = darkModeButton.IsOn;
    }
}
