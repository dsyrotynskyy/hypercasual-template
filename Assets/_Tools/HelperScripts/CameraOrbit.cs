﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbit : MonoBehaviour {

    [Header("Rotation")]
    public float rotationSmooth = 4.0f;                    //how smooth the camera moves into place
    public float horizontalSpeed = 1f;
    public float verticalSpeed = 12f;

    [Header("Vertical Range")]
    public float verticalRotationMin = 50f;
    public float verticalRotationMax = 100f;

    [Header("Zoom Properties")]
    float _zoom;                     //how far the camera is from the player.

    public float minZoom = 15;                //min camera distance
    public float maxZoom = 50;                //max camera distance
    public float zoomSpeed = 12f;
    public float multitouchZoomSpeed = 0.1f;

    float _horizontalRotationY = 70f;            //the angle at which you will rotate the camera (on an axis)
    float _verticalRotationX = 55f;

    [Header("Player to follow")]
    public Transform target;                    //the target the camera follows

    [Header("Layer(s) to include")]
    public LayerMask CamOcclusion;                //the layers that will be affected by collision

    //[Header("Map coordinate script")]
    ////    public worldVectorMap wvm;
    RaycastHit hit;


    //    Vector3 camPosition;
    Vector3 camMask;
    Vector3 followMask;

    // Use this for initialization
    void Start()
    {
        _horizontalRotationY = target.eulerAngles.y - 45f;
        _verticalRotationX = target.eulerAngles.x;
    }

    void LateUpdate()
    {
        PerformCameraMovement();
        CalculateRotation();
        CalculateZoom();
    }

    bool _wasMultitouch;
    Vector3 _mouseLastPos;

    Vector2 CalculateRotationDelta(out bool rotationChanged)
    {
        if (Input.GetMouseButtonDown(0))
        {
            _mouseLastPos = Input.mousePosition;
        } 

        if (Input.touchCount >= 2 || !Input.GetMouseButton(0)) {
            _wasMultitouch = true;
            _mouseLastPos = Input.mousePosition;
            rotationChanged = false;
            return Vector2.zero;
        }

        if (_wasMultitouch)
        {
            _wasMultitouch = false;
            _mouseLastPos = Input.mousePosition;
            rotationChanged = false;
            return Vector2.zero;
        }

        _wasMultitouch = false;
        rotationChanged = true;

        Vector3 delta = Input.mousePosition - _mouseLastPos;
        _mouseLastPos = Input.mousePosition;

        var h = delta.x * horizontalSpeed * _zoom;
        var v = delta.y * verticalSpeed;

        float camRotateModif = 180f * 0.02f;
    
        return new Vector2(h, v) * Time.deltaTime * camRotateModif;
    }

    void PerformCameraMovement ()
    {
        Vector3 targetOffset = new Vector3(target.position.x, (target.position.y + 2f), target.position.z);
        Quaternion rotation = Quaternion.Euler(_verticalRotationX, _horizontalRotationY, 0f);
        Vector3 vectorMask = Vector3.one;
        Vector3 rotateVector = rotation * vectorMask;

        float _distanceUp = -2;                    //how high the camera is above the player    
        Vector3 camPosition;
        camPosition = targetOffset + Vector3.up * _distanceUp - rotateVector * _zoom;
        camMask = targetOffset + Vector3.up * _distanceUp - rotateVector * _zoom;

        transform.position = SmoothCamMethod(camPosition, false);
//        transform.position = camPosition;
        transform.LookAt(target);
    }

    float HorizontalAxis = 0f;
    float VerticalAxis = 0f;

    void CalculateRotation ()
    {
        HorizontalAxis = 0f;
        VerticalAxis = 0f;

        bool rotationChanged;
        var res = CalculateRotationDelta(out rotationChanged);
        if (rotationChanged) {
            HorizontalAxis = res.x;
            VerticalAxis = res.y;
        } 

        _horizontalRotationY += HorizontalAxis;
        _horizontalRotationY = ClampAngle(_horizontalRotationY);

        _verticalRotationX -= VerticalAxis;
        _verticalRotationX = Mathf.Clamp(_verticalRotationX, verticalRotationMin, verticalRotationMax);

    }

    float ClampAngle (float v)
    {
        if (v > 360)
        {
            v = 0f;
        }
        else if (v < 0f)
        {
            v = (v + 360f);
        }

        return v;
    }

    void CalculateZoom()
    {
        float mouseScroll = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

        if (Mathf.Abs(mouseScroll) < Mathf.Epsilon)
        {
            mouseScroll = GetMultitouchZoom() * multitouchZoomSpeed;
        }

        _zoom = Mathf.Clamp(_zoom += mouseScroll, minZoom, maxZoom);
    }

    float GetMultitouchZoom()
    {
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            return deltaMagnitudeDiff;
        }
        return 0f;
    }

    Vector3 SmoothCamMethod(Vector3 camPosition, bool isCollision)
    {
        if (isCollision)
        {
            return camPosition;
        }

        return Vector3.Lerp(transform.position, camPosition, Time.deltaTime * rotationSmooth);
    }

    Vector3 OccludeRay(Vector3 camPosition, ref Vector3 targetFollow, ref bool isCollision)
    {
        RaycastHit wallHit = new RaycastHit();
        //linecast from your player (targetFollow) to your cameras mask (camMask) to find collisions.
        if (Physics.Linecast(targetFollow, camMask, out wallHit, CamOcclusion))
        {
            //the smooth is increased so you detect geometry collisions faster.
            rotationSmooth = 10f;
            //the x and z coordinates are pushed away from the wall by hit.normal.
            //the y coordinate stays the same.
            camPosition = new Vector3(wallHit.point.x + wallHit.normal.x * 0.5f, camPosition.y, wallHit.point.z + wallHit.normal.z * 0.5f);
            isCollision = true;
        }
        else
        {
            isCollision = false;
        }

        isCollision = false;

        return camPosition;
    }
}
