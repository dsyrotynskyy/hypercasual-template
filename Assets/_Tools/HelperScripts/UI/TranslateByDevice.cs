﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateByDevice : MonoBehaviour {
	public Vector2 iphoneXTranslate= Vector3.zero;

	void Start () {
        CheckIphoneX();
	}

    void CheckIphoneX () {
        if (DeviceHelper.IsIPhoneX) {
            RectTransform rect = GetComponent<RectTransform>();
            rect.anchoredPosition += iphoneXTranslate ;
        } 
    }
}
