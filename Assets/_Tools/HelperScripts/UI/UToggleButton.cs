﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UToggleButton : MonoBehaviour
{
    public Button button;
    public Image targetImage;
    public Sprite onSprite;
    public Sprite offSprite;
    public TemplateEvent<bool> onValueChangedEvent = new TemplateEvent<bool>();

    bool _isOn;
    public bool IsOn
    {
        get { return _isOn; }
        set
        {
            _isOn = value;

            UpdateUI();

            onValueChangedEvent.Invoke(_isOn);
        }
    }

    void Start()
    {
        button.onClick.AddListener(ToggleValue);
        UpdateUI();
    }

    private void ToggleValue()
    {
        IsOn = !IsOn;
    }

    void UpdateUI()
    {
        if (IsOn)
        {
            targetImage.sprite = onSprite;
        }
        else
        {
            targetImage.sprite = offSprite;
        }
    }
}
