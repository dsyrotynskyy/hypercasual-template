﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "_SoundStorage", menuName = "Data/SoundStorage")]
public class SoundStorage : ScriptableObject {

    public AudioClip buttonSound;
}
