﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class USoundOnClick : MonoBehaviour {

    [Inject]
    SoundStorage uiSoundManager;

    [Inject]
    AudioPlayer _audioPlayer;

    protected Button _button;

    protected virtual void Awake () {
        _button = GetComponent<Button>();

        if (!_button) {
            Debug.LogError ("Missing Button script on " + name);
            return;
        }

        _button.onClick.AddListener(OnClickAction);
    }

    private void OnClickAction()
    {
        _audioPlayer.Play(uiSoundManager.buttonSound);
    }
}
