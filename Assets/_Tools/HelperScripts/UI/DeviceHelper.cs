﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DeviceHelper  {

	static bool _inited = false;
	static bool isIPhoneX = false;
    static float iPhoneXRation;
    static float actualRation;
    static int IPHONE_X_WIDTH = 1125;

    static float actualToIPhoneX;

    static void Init ()
    {
        iPhoneXRation = (float)2436 / IPHONE_X_WIDTH;
        actualRation = (float)Screen.height / Screen.width;

        int i1 = (int)(iPhoneXRation * 100f);
        int i2 = (int)(actualRation * 100f);

        if (i1 <= i2)
        {
            isIPhoneX = true;
        }
        else
        {
            isIPhoneX = false;
        }

        actualToIPhoneX = ((float)i2) / i1;

        _inited = true;
    }

    static void TryInit()
    {
        if (!_inited)
        {
            Init();
        }
    }

    public static float GetActualRatioToIPhoneX
    {
        get
        {
            TryInit();
            return actualToIPhoneX;
        }
    }

    public static bool IsIPhoneX {
		get {
            TryInit();

            bool res;
            res = isIPhoneX;
			return res;
		}
	}
}
