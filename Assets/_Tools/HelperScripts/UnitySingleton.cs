﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class UnitySingleton<T> : SingletonBehaviour<T> where T : MonoBehaviour
{
    public static bool HasInstance
    {
        get
        {
            return _instance;
        }
    }

    public static T GetInstance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogWarning("Instance not initialized! Have to seek it! WARNING! Bad architecture!");
                _instance = (T)FindObjectOfType(typeof(T));

                if (_instance == null)
                {
                    Debug.LogError("Not found instance of SINGLETON object!!!");
                }
            }

            return _instance;
        }
    }

    public static T GetOrCreateInstance
    {
        get
        {
			if (_instance == null)
			{
				GameObject go = new GameObject();
				_instance = go.AddComponent<T>();
				_instance.name = "(singleton) " + typeof(T).ToString();
			}

            return _instance;
        }
    }
}
