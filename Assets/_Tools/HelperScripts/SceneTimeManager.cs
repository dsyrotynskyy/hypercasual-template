﻿using UnityEngine;
using System.Collections;
using System;

public class SceneTimeManager : SingletonBehaviour<SceneTimeManager> {
    private int _time = 10;

    private TimeScaleValue _brakerTimeScale = new TimeScaleValue();

    private bool _pause = false;

    float _baseFixedDeltaTime = 0.02f;
    float _baseTimeScale = 1f;
    public float GameTime { get; private set; }

    protected override void OnAwake()
    {
        base.OnAwake();
        _baseFixedDeltaTime = Time.fixedDeltaTime;
        _baseTimeScale = Time.timeScale;
    }

	void Update () {
        if (_pause)
        {
            return;
        }

        float deltaTime = Time.deltaTime;
        GameTime += deltaTime;
        float timeAccum = 1000 * deltaTime;
        _time += (int)timeAccum;

        if (_brakerTimeScale.OnUpdate())
        {
            UpdateTimeScale();
        }
    }

    public void SetPause (bool pause)
    {
        _pause = pause;
        if (pause)
        {
            Time.timeScale = 0f;
        } else
        {
            UpdateTimeScale();
        }
    }

    public bool IsPause
    {
        get
        {
            return _pause;
        }
    }

    public int CurrentTime
    {
        get
        {
            return _time;
        }
    }

    protected override bool OrderDontDestroyOnLoad
    {
        get
        {
            return false;
        }
    }

    internal void OrderTimeBrake(float timeScaleSpeedBrakePower, float timeScaleSpeedBrakePowerPeriod)
    {
        _brakerTimeScale.OrderTimeScale(timeScaleSpeedBrakePower, 1f, timeScaleSpeedBrakePowerPeriod);
        UpdateTimeScale();
    }

    void UpdateTimeScale ()
    {
        Time.timeScale = this._brakerTimeScale.Power;
        Time.fixedDeltaTime = this._baseFixedDeltaTime * Time.timeScale;
    }

    #region TimeBraker

    class TimeScaleValue
    {
        float _curPower = 1f,
            _desPower = 1f,
            _startPower = 1f, 
            _period,
            _curTime;

        bool _lerpingTimeScale;

        public void OrderTimeScale (float desiredPower)
        {
            this._curPower = desiredPower;
            this._desPower = desiredPower;
            this._startPower = desiredPower;

            this._curTime = 0f;
            this._period = -1f;
            this._lerpingTimeScale = false;
        }

        public void OrderTimeScale (float startPower, float normalPower, float period)
        {
            this._startPower = startPower;
            this._curPower = startPower;
            this._desPower = normalPower;

            this._curTime = 0f;
            this._period = period;
            this._lerpingTimeScale = true;
        }

        public bool OnUpdate ()
        {
            if (!_lerpingTimeScale)
            {
                return false;
            }

            float delta = Time.deltaTime / Time.timeScale;
            _curTime += delta;
            float percent = _curTime / _period;
            if (percent < 1f)
            {
                _curPower = Mathf.Lerp(_startPower, _desPower, percent);
            } else
            {
                OrderTimeScale(_desPower);
            }

            return true;
        }

        public float Power {
            get
            {
                return _curPower;
            }
        }
    }

    #endregion

    void OnDestroy ()
    {
        Time.fixedDeltaTime = _baseFixedDeltaTime;
        Time.timeScale = _baseTimeScale;
    }

    #region Timer
    public ActivityTimer CreateActivityTimer(float timeOfActivity, bool startActive)
    {
        int val = (int)(timeOfActivity * 1000);
        ActivityTimer t = new ActivityTimer(this, val, startActive);
        return t;
    }

    public FrequencyTimer CreateFrequencyTimer(float frequency)
    {
        int val = (int)(frequency * 1000);
        FrequencyTimer t = new FrequencyTimer(this, val);
        return t;
    }
    #endregion
}

public class FrequencyTimer
{
    protected int firstTimeCall = -1;
    protected int prevTickCount;
    protected int currentTick;

    /*
	 * if timeOfActivity=-1 equals infinity
	 * */
    protected int _frequency;

    protected SceneTimeManager _timeManager;

    public FrequencyTimer(SceneTimeManager timeManager, int timeFrequency)
    {
        _frequency = timeFrequency;
        _timeManager = timeManager;
        this.firstTimeCall = TickCount;
    }

    public bool CheckEnoughTimeLeft()
    {
        currentTick = TickCount;

        bool enoughTimeLeft = false;

        if (GetFrequencyOffset() > _frequency)
        {
            enoughTimeLeft = true;
        }

        return enoughTimeLeft;
    }

    public int GetFrequencyOffset()
    {
        return currentTick - prevTickCount;
    }

    public bool EnoughTimeLeft()
    {
        bool enoughTimeLeft = CheckEnoughTimeLeft();
        if (enoughTimeLeft)
        {
            prevTickCount = currentTick;
        }

        return enoughTimeLeft;
    }

    public float GetFrequencyProgress()
    {
        currentTick = TickCount;
        float offset = currentTick - prevTickCount;
        return offset * 100 / _frequency;
    }

    public void Restart()
    {
        this.firstTimeCall = TickCount;
        prevTickCount = this.firstTimeCall;
    }

    public void ForceTime()
    {
        prevTickCount -= _frequency;
    }

    protected int TickCount
    {
        get
        {
            return _timeManager.CurrentTime;
        }
    }
}

public class ActivityTimer
{
    protected int _firstTimeCall = -1;
    protected int _currentTick;

    /*
     * if timeOfActivity=-1 equals infinity
     * */
    protected int _timeOfActivity;

    protected SceneTimeManager _timeManager;

    public ActivityTimer(SceneTimeManager timeManager, int timeOfActivity, bool startActive)
    {
        _timeOfActivity = timeOfActivity;
        _timeManager = timeManager;
        this._firstTimeCall = TickCount;
        if (!startActive)
        {
            this.Deactivate();
        }
    }

    public void SetNewLifePeriod(int lifePeriod)
    {
        this._timeOfActivity = lifePeriod;
        this.Restart();
    }

    public void Restart()
    {
        this._firstTimeCall = TickCount;
    }

    public bool IsActive
    {
        get
        {
            if (_timeOfActivity < 0)
            {
                return true;
            }
            _currentTick = TickCount;
            int timeOffset = _currentTick - _firstTimeCall;
            return timeOffset < _timeOfActivity;
        }
    }

    public void Deactivate()
    {
        _firstTimeCall = _firstTimeCall - _timeOfActivity;
    }

    public float GetProgressPercent()
    {
        if (!IsActive)
        {
            return 0;
        }

        _currentTick = TickCount;
        int timeOffset = _currentTick - _firstTimeCall;

        return timeOffset * 100 / _timeOfActivity;
    }

    public int TimeOfActivity
    {
        get
        {
            return _timeOfActivity;
        }
    }

    public int MiliSecondsOfLife
    {
        get
        {
            _currentTick = TickCount;
            int timeOffset = _currentTick - _firstTimeCall;
            return _timeOfActivity - timeOffset;
        }
    }

    protected int TickCount
    {
        get
        {
            return _timeManager.CurrentTime;
        }
    }
}
