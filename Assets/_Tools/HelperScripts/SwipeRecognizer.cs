﻿using UnityEngine;
using System.Collections;
using System;

public enum EDirection
{
    Up = 0,
    Down = 1,
    Right = 2,
    Left = 3,
    None = 4
}

public class SwipeRecognizer : MonoBehaviour
{
    public enum SwipeState
    {
        Prepared, Performed, Released
    }

    SwipeState swipeState = SwipeState.Released;

    public event Action<Vector2, EDirection> OnSwipeDotsContinue;
    public event Action<Vector2, EDirection> OnSwipeInchesContinue;
    public event Action<EDirection> OnSwipeEnded;

    protected EDirection SwipeDirection { get; private set; }

	public static float DeviceDiagonalSizeInInches()
	{
		float screenWidth = Screen.width / Screen.dpi;
		float screenHeight = Screen.height / Screen.dpi;
		float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));

		return diagonalInches;
	}

    public void Reset()
    {
        swipeState = SwipeState.Released;
    }

    void LateUpdate()
    {
        if (swipeState == SwipeState.Released)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButton (0))
            {
                fingerStart = Input.mousePosition;
                fingerEnd = fingerStart;
                swipeState = SwipeState.Prepared;
            }
        } else if (swipeState == SwipeState.Prepared)
        {
            if ((Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)))
            {              

                ContinueSwipe();
            }
        }
        else if (swipeState == SwipeState.Performed)
        {
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))
        {
        }
        else
        {
            if (swipeState != SwipeState.Released)
            {
                ContinueSwipe();

                if (OnSwipeEnded != null)
                {
					var direction = SwipeDirection;
					direction = GetSwipeDirection (GetMouseDelta);
					OnSwipeEnded(direction);
                }
                swipeState = SwipeState.Released;
				fingerStart = Vector2.zero;
				fingerEnd = Vector2.zero;
            }
        }
    }

    bool SelectInitialSwipeDirection ()
    {
        Vector2 mouseDelta = GetMouseDelta;

        SwipeDirection = GetSwipeDirection(mouseDelta);
		return true;
    }

    bool SwipeDisabled ()
    {
        if (DeviceHelper.IsIPhoneX)
        {
            var mousePosition = Input.mousePosition;
            float pixelsFromTop = Screen.height - mousePosition.y;
            float inchesFromTop = pixelsFromTop / Screen.dpi;

            if (inchesFromTop < 0.5f)
            {
                return true;
            }

            float inchesFromDown = mousePosition.y / Screen.dpi;

            if (inchesFromDown < 0.25f)
            {
                return true;
            }
        }

        return false;
    }

    void ContinueSwipe ()
    {
        if (SwipeDisabled()) {
            return;
        }

        Vector2 delta = Vector2.zero;

        Vector2 mouseDelta = GetMouseDelta;
        Vector2 normalizedDelta = NormalizeMouseDelta(mouseDelta);
        SwipeDirection = GetSwipeDirection(normalizedDelta);
        delta = mouseDelta;

        if (OnSwipeDotsContinue != null) {
			OnSwipeDotsContinue(delta, SwipeDirection);
		}

        Vector2 swipeActual = delta;

        swipeActual = swipeActual / Screen.height * 24.5f;

		if (OnSwipeInchesContinue != null) {
			OnSwipeInchesContinue(swipeActual, SwipeDirection);
		}
    }

    Vector2 NormalizeMouseDelta (Vector2 actualDelta)
    {
        return actualDelta;
    }

    public Vector2 GetMouseDelta
    {
        get
        {
            fingerEnd = Input.mousePosition;
            Vector2 direction = fingerEnd - fingerStart;
            return direction;
        }
    }

    EDirection GetSwipeDirection(Vector2 direction)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            if (direction.x > 0)
                return (EDirection.Right);
            else
                return (EDirection.Left);
        }
        else {
            if (direction.y > 0)
                return (EDirection.Up);
            else
                return (EDirection.Down);
        }
    }

    Vector2 fingerStart;
    Vector2 fingerEnd;
}
