using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent( typeof( ScrollRect ) )]
public class ScrollToSelected : MonoBehaviour {

	public float scrollSpeed = 10f;

	ScrollRect m_ScrollRect;
	RectTransform m_RectTransform;
	RectTransform m_ContentRectTransform;
	RectTransform m_SelectedRectTransform;

	void Awake() {
		m_ScrollRect = GetComponent<ScrollRect>();
		m_RectTransform = GetComponent<RectTransform>();
		m_ContentRectTransform = m_ScrollRect.content;
	}

	public void CenterOnItem(RectTransform selected, float yOffset) {

		// grab the current selected from the eventsystem
//		GameObject selected = EventSystem.current.currentSelectedGameObject;

		if ( selected == null ) {
			return;
		}
		if ( selected.transform.parent != m_ContentRectTransform.transform ) {
			selected.SetParent (m_ContentRectTransform.transform);
		}

		m_SelectedRectTransform = selected;

		// math stuff
		Vector3 selectedDifference = m_RectTransform.localPosition - m_SelectedRectTransform.localPosition;
		float contentHeightDifference = ( m_ContentRectTransform.rect.height - m_RectTransform.rect.height );

		float selectedPosition = ( m_ContentRectTransform.rect.height - selectedDifference.y );
		float currentScrollRectPosition = m_ScrollRect.normalizedPosition.y * contentHeightDifference;
		float above = currentScrollRectPosition - ( m_SelectedRectTransform.rect.height / 2 ) + m_RectTransform.rect.height;
		float below = currentScrollRectPosition + ( m_SelectedRectTransform.rect.height / 2 );

		float newNormalizedY;
		float step;
		bool shouldMove = false;
		// check if selected is out of bounds
		if ( selectedPosition > above ) {
			step = selectedPosition - above;
		} else 
//		if	( selectedPosition < below )
		{
			step = selectedPosition - below;
		}

		float newY = currentScrollRectPosition + step;
		newY = selectedPosition - yOffset;
		newNormalizedY = newY / contentHeightDifference;
		newNormalizedY = Mathf.Clamp (newNormalizedY, 0f, 1f);

		Vector2 targetPosition = new Vector2 (0, newNormalizedY);
		m_ScrollRect.normalizedPosition = targetPosition;
	}
}