﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SingletonBehaviour <T> : MonoBehaviour where T : MonoBehaviour {

    private void Awake()
    {
        if (!CheckSingleton())
        {
            return;
        }

        if (OrderDontDestroyOnLoad)
        {
            if (gameObject.transform.parent != null)
            {
                gameObject.transform.SetParent(null);
            }
            DontDestroyOnLoad(gameObject);
            //            print (gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        OnAwake();
    }

    bool CheckSingleton()
    {
        if (_instance != null)
        {
            if (_instance != this)
            {
                Debug.Log("Instance already exist; Destroying " + name);
                Destroy(gameObject);
                return false;
            }
            return true;
        }
        else
        {
            _instance = (T)((System.Object)this);
            return true;
        }
    }

    protected static T _instance;

    protected virtual void OnAwake()
    {

    }

    protected virtual bool OrderDontDestroyOnLoad
    {
        get
        {
            return false;
        }
    }

    protected virtual void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
    }

    protected virtual void OnDestroy()
    {
        _instance = null;
    }
}
