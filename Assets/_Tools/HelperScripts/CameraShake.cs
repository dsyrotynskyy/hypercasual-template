﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CameraShake : MonoBehaviour
{
    [HideInInspector]
    Transform camTransform;

    private float shakeTime = 0f;
    private float shakePower = 0.7f;

    public float decreaseFactor = 1.0f;

    Vector3 originalPos;

    //protected override void OnAwake()
    //{
    //    base.OnAwake();

    //}

    private void Start()
    {
        if (camTransform == null)
        {
            camTransform = transform;
        }
        originalPos = camTransform.localPosition;
    }

    void Update()
    {
        if (shakeTime > 0 && _targetTime > 0)
        {
            Vector3 newPos = originalPos + UnityEngine.Random.insideUnitSphere * shakePower * Mathf.Sqrt(shakeTime) * shakeTime / _targetTime;
            LerpLocalPos(newPos, Time.deltaTime * 20f);
            shakeTime -= Time.deltaTime * decreaseFactor;
//            Debug.Log("Newpos = " + newPos);
        }
        else
        {
            shakeTime = 0f;
            LerpLocalPos(originalPos, Time.deltaTime * 20f);
            isDoingBigShake = false;
        }
    }

    void LerpLocalPos(Vector3 target, float step)
    {
        camTransform.localPosition = Vector3.Lerp(camTransform.localPosition, target, step);
    }

    float _targetTime;

    public void OrderShake(float newShakePower, float newShakeTime)
    {
        _targetTime = newShakeTime;

        this.shakePower = newShakePower;
        this.shakeTime = newShakeTime;
    }

	public void PlayDragonShake () {
//		this.Invoke ("PlayDragonShakeSmall", 0f);
		this.Invoke ("PlayDragonShakeMedium", 0f);
		this.Invoke ("PlayDragonShakeBig", 0.8f);
	}

	void PlayDragonShakeBig () {
		isDoingBigShake = true;
		OrderShake(1.0f, 2f);
	}

	void PlayDragonShakeMedium () {
		isDoingBigShake = true;
		OrderShake(0.7f, 4f);
	}

	void PlayDragonShakeSmall () {
		isDoingBigShake = true;
		OrderShake(0.2f, 4f);
	}

    [Header("Small Shake")]
    public float smallShakePower = 0.2f;
    public float smallShakeDuration = 0.1f;

    public void PlaySmallShake()
    {
        if (isDoingBigShake) {
            return;
        }
        OrderShake(smallShakePower, smallShakeDuration);
    }

    [Header("Fever Shake")]
    public float feverShakePower = 0.4f;
    public float feverShakeDuration = 0.4f;

    public void PlayFeverShake()
    {
        if (isDoingBigShake)
        {
            return;
        }
        OrderShake(feverShakePower, feverShakeDuration);
    }

    public void PlayPerfectShake()
    {
		if (isDoingBigShake)
		{
			return;
		}
        OrderShake(2.0f, 0.73f);
    }

    bool isDoingBigShake = false;

    [Header("Fail Shake")]
    public float failShakePower = 0.5f;
    public float failShakeDuration = 0.5f;

    public void PlayFailShake()
    {
        isDoingBigShake = true;
        OrderShake(failShakePower, failShakeDuration);
    }

    static CameraShake _instance;

    public static CameraShake GetInstance
    {
        get
        {
            return _instance;
        }
    }

    //private void OnEnable()
    //{

    //}

    private void FixedUpdate()
    {
        _instance = this;
    }
}

