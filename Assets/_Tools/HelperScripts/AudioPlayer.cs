﻿// /////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audio Manager.
//
// This code is release under the MIT licence. It is provided as-is and without any warranty.
//
// Developed by Daniel Rodríguez (Seth Illgard) in April 2010
// http://www.silentkraken.com
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class AudioPlayer : SingletonBehaviour<AudioPlayer>
{
    public AudioMixer audioMixer;
    public UnityEngine.Audio.AudioMixerGroup soundMixerGroup;
    Camera _mainCamera;
    AudioMixerGroup _defaultAudioMixerGroup;

    protected override void OnAwake()
    {
        base.OnAwake();
        _mainCamera = Camera.main;
        var s = soundMixerGroup;
        _defaultAudioMixerGroup = soundMixerGroup;
    }

    public void SetVolume(bool volume)
    {
        audioMixer.EnableAudio("SoundVolume", volume);
    }

    protected override void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        base.OnSceneLoaded(arg0, arg1);
        _mainCamera = Camera.main;
    }

    protected override bool OrderDontDestroyOnLoad
    {
        get
        {
            return true;
        }
    }

    public AudioSource PlayLoop(AudioClip clip)
    {
		if (!_mainCamera) {
            _mainCamera = Camera.main;
		}
        return Play(clip, _mainCamera.transform, 1f, 1f, true);
    }

    public AudioSource Play(AudioClip clip)
    {
		if (!_mainCamera) {
			Debug.LogError ("Main Camera destroyed !!! Cannot play sound !!!");
			return null;
		}
        return Play(clip, _mainCamera.transform);
    }

    public AudioSource Play(AudioClip clip, Transform emitter)
    {
        return Play(clip, emitter, 1f, 1f);
    }

    public AudioSource Play(AudioClip clip, Transform emitter, float volume)
    {
        return Play(clip, emitter, volume, 1f);
    }

    AudioClip prevClip;
	int prevFrameCount;

    public AudioSource Play(AudioClip clip, Transform emitter, float volume, float pitch, bool loop = false)
    {
		if (!clip) {
			Debug.LogError("Error! AudioClip = null");
			return null;
		}

		int newFrameCount = Time.frameCount;
		if (prevFrameCount == newFrameCount) {
			if (prevClip == clip) {
				return null;
			}
		} else {
			prevFrameCount = newFrameCount;
		}
			
        prevClip = clip;

        GameObject go = new GameObject("Audio: " + clip.name );
        go.transform.position = emitter.position;
        go.transform.SetParent(emitter);

        AudioSource source = go.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.pitch = pitch;
        source.outputAudioMixerGroup = _defaultAudioMixerGroup;
        source.Play();
        if (loop) {
            source.loop = true;
        } else {
            Destroy(go, clip.length * Time.timeScale);
        }
        return source;
    }

    public AudioSource Play(AudioClip clip, Vector3 point)
    {
        return Play(clip, point, 1f, 1f);
    }

    public AudioSource Play(AudioClip clip, Vector3 point, float volume)
    {
        return Play(clip, point, volume, 1f);
    }

    /// <summary>
    /// Plays a sound at the given point in space by creating an empty game object with an AudioSource
    /// in that place and destroys it after it finished playing.
    /// </summary>
    /// <param name="clip"></param>
    /// <param name="point"></param>
    /// <param name="volume"></param>
    /// <param name="pitch"></param>
    /// <returns></returns>
    public AudioSource Play(AudioClip clip, Vector3 point, float volume, float pitch)
    {
        //Create an empty game object
        GameObject go = new GameObject("Audio: " + clip.name);
        go.transform.position = point;

        //Create the source
        AudioSource source = go.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.pitch = pitch;
        source.Play();
        Destroy(go, clip.length);
        return source;
    }
}