﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpFollower : MonoBehaviour {

    public float lerpPower = 5f;
    public Transform target;
	
	void Update () {
        float t = Time.deltaTime * lerpPower;
        transform.position = Vector3.Lerp(transform.position, target.transform.position, t);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, t);
	}
}
