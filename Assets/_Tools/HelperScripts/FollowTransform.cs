﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour {

	public Transform _target;

	void Start () {
		UpdatePosition ();
	}
	
	void Update () {
		UpdatePosition ();
	}

	void UpdatePosition () {
		transform.position = _target.transform.position;
	}
}
