﻿using UnityEngine;

public class CSVReader
{
    static public void Read(TextAsset file, ReadCSVLine readCSVLine, ParseCSVToLines parseCSVToLines, int startLine)
    {
        string[] lines = parseCSVToLines(file);

        for (int i = 0; i < lines.Length; i++)
        {
            if (i >= startLine)
            {
                string line = lines[i];
                if (!string.IsNullOrEmpty(line))
                {
                    try
                    {
                        string[] array = line.Split(',');
                        if (array.Length == 0 || array[0] == "end")
                        {
                            break;
                        }

                        string firstElement = array[0];

                        readCSVLine(array, i);
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError(e.ToString());
                    }
                }
            }
        }
    }

    static public void Read(TextAsset file, ReadCSVLine readCSVLine)
    {
//        ParseCSVToLines parseCSVToLines = ReadLines;

        Read(file, readCSVLine, ReadLines, 1);
    }

    static public string[] ReadLines(TextAsset textAsset)
    {
        string[] lines = textAsset.text.Split("\n"[0]);
        return lines;
    }

    public delegate string[] ParseCSVToLines(TextAsset textAsset);
    public delegate void ReadCSVLine(string[] line, int i);
}
