using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Linq;

public class Loom : MonoBehaviour
{
	public static int MAX_THREADS = 8;
	static int numThreads;
	
	private static Loom _current;
	public static Loom Current
	{
		get
		{
			Initialize();
			return _current;
		}
	}
	
	void OnEnable()
	{
		_current = this;
        if (_current != this)
        {
            Debug.Log ("Destroying other instance of LOOM on " + gameObject.name);
            Destroy(this);
        }
	}
	
	public static void Initialize()
	{
		if (!_current)
		{		
			if(!Application.isPlaying) {
				return;
			}
		
			_current = FindObjectOfType<Loom> ();
			if (!_current) {
				var g = new GameObject("Loom");
				_current = g.AddComponent<Loom>();
			}
		}		
	}
	
	private List<Action> _actions = new List<Action>();
	public struct DelayedQueueItem
	{
		public float time;
		public Action action;
	}
	private List<DelayedQueueItem> _delayed = new  List<DelayedQueueItem>();

	List<DelayedQueueItem> _currentDelayed = new List<DelayedQueueItem>();
	
	public static void QueueOnMainThread(Action action)
	{
		QueueOnMainThread( action, 0f);
	}
	static void QueueOnMainThread(Action action, float time)
	{
        if (!_current)
        {
            Debug.LogError("Missing Loom GO");
            return;
        }
		if(Math.Abs(time) > 0)
		{
			lock(Current._delayed)
			{
				Current._delayed.Add(new DelayedQueueItem { time = Time.time + time, action = action});
			}
		}
		else
		{
			lock (Current._actions)
			{
				Current._actions.Add(action);
			}
		}
	}
	
	public static Thread RunAsync(Action a, bool fromMainThread)
	{
		Initialize();
        if (!fromMainThread)
        {
            while (numThreads >= MAX_THREADS)
            {
                Thread.Sleep(1);
            }
        }

		Interlocked.Increment(ref numThreads);
		ThreadPool.QueueUserWorkItem(RunAction, a);
		return null;
	}
	
	private static void RunAction(object action)
	{
		try
		{
			((Action)action)();
		}
		catch
		{
		}
		finally
		{
			Interlocked.Decrement(ref numThreads);
		}
			
	}

    List<DelayedQueueItem> _otherDelayed = new List<DelayedQueueItem>();

    public static void QueueDelayed(Action action, float time)
    {
        if (!_current)
        {
            Debug.LogError("Missing Loom GO");
            return;
        }
       Current._otherDelayed.Add(new DelayedQueueItem { time = Time.time + time, action = action });
    }

    void OnDisable()
	{
		if (_current == this)
		{
			
			_current = null;
		}
	}
		
	List<Action> _currentActions = new List<Action>();
	
	void Update()
	{
		lock (_actions)
		{
            _currentActions.Clear();

            if (_actions.Count > 0)
            {
                _currentActions.AddRange(_actions);
                _actions.Clear();
            }
		}

        for (int i = 0; i < _currentActions.Count; i++)
        {
            var a = _currentActions[i];
            a();
        }

		lock(_delayed)
		{
            _currentDelayed.Clear();

            if (_delayed.Count > 0)
            {
                _currentDelayed.AddRange(_delayed.Where(d => d.time <= Time.time));
                for (int i = 0; i < _currentDelayed.Count; i++)
                {
                    var item = _currentDelayed[i];
                    _delayed.Remove(item);
                }
            }
		}

        for (int i = 0; i < _currentDelayed.Count; i++)
        {
            var delayed = _currentDelayed[i];
            delayed.action();
        }

        for (int i = _otherDelayed.Count - 1; i >= 0; i--)
        {
            if (i >= _otherDelayed.Count)
            {
                continue;
            }
            var delayed = _otherDelayed[i];
            if (delayed.time <= Time.time)
            {
                delayed.action();
                _otherDelayed.Remove(delayed);
            }
            
        }
    }
}

