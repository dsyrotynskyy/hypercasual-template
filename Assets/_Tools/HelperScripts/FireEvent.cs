﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEvent {

    List<Action> actions = new List<Action>();
	
    public void AddListener (Action action) {
        if (Invoked) {
            action();
        } else {
            actions.Add(action);
        }
    }
	
    public void Invoke () {
        if (!Invoked) {
            if (actions.Count > 0)
            {
                foreach (var a in actions)
                {
                    a();
                }
            }

            Invoked = true;
        }
    }

    public void RemoveListener(Action action)
    {
        if (actions.Count > 0)
        {
            actions.Remove(action);
        }
    }

    bool Invoked { get; set; }

    public void Reset ()
    {
        Invoked = false;
        actions.Clear();
    }
}
