using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenFader : MonoBehaviour
{
    public Canvas renderingCanvas;
	public float FadeSpeed = 0f;          // Speed that the screen fades to and from black.
		
	public Image target;

    void Start ()
    {
        StartFadeOut();
    }

	bool isFadeOut = false;
	
	void SetAlpha (float na) {
		Color c = this.target.color;
		c.a = na;
		target.color = c;
        currentAlpha = na;
	}

    public void StartFadeIn()
    {
        isFadeOut = false;
        SetActiveRenderer(true);
        this.SetAlpha(0f);
    }

    public void StartFadeOut ()
	{
        SetActiveRenderer(true);
        SetAlpha(1f);
        isFadeOut = true;
	}
	
	float currentAlpha;

    void Update () {
		if (!target) {
			return;
		}
        float targetAlpha;
		if (isFadeOut) {
            targetAlpha = 0f;
        } else {
            targetAlpha = 1f;
        }
        float speed = Time.deltaTime * FadeSpeed / 10f;
        currentAlpha = Mathf.MoveTowards(currentAlpha, targetAlpha, speed);
        SetAlpha (currentAlpha);

        if (isFadeOut && currentAlpha == 0f)
        {
            SetActiveRenderer(false);
        }

        if (!isFadeOut && currentAlpha >= 1f)
        {
            onFadeIn.Invoke();
            this.enabled = false;
        }
    }

    void SetActiveRenderer(bool active)
    {
        gameObject.SetActive(active);
//        renderingCanvas.enabled = active;
    }

    public UnityEvent onFadeIn;
}

