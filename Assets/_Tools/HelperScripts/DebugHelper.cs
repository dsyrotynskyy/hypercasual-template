﻿using UnityEngine;
using System;

public static class DebugHelper
{
    static bool _inited = false;
    static bool _isDebugBuild = false;

    public static void SetDebugBuild(bool isDebug)
    {
        _inited = true;
        _isDebugBuild = isDebug;
    }

    public static bool isDebugBuild
    {
        get
        {
            bool res = Debug.isDebugBuild;
            if (!Debug.isDebugBuild)
            {
                if (_inited)
                {
                    res = _isDebugBuild;
                }
            }
            return res;
        }
    }
}
