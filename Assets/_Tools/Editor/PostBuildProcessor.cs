﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;
using System.Linq;
using System.Collections.Generic;

public class PostBuildProcessor : MonoBehaviour
{
    public static void PreExport()
    {
        Debug.Log("Cloud build pre export");
    }

    public static void PostExport(string exportPath)
    {
        Debug.Log("Cloud build post export");
        ProcessPostBuild(BuildTarget.iOS, exportPath);
    }

    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {
        Debug.Log("[LOCAL BUILD] OnPostprocessBuild");
        ProcessPostBuild(buildTarget, path);
    }

    private static void ProcessPostBuild(BuildTarget buildTarget, string path)
    {
#if UNITY_IOS
        EnableModules(buildTarget, path);
        DisableBitCode(buildTarget, path);

        ProcessPostBuildPityTry(buildTarget, path);
#endif
    }

    private static void EnableModules(BuildTarget buildTarget, string path)
    {
#if UNITY_IOS
        Debug.Log("[UNITY_IOS] ProcessPostBuild - Xcode Manipulation API");
        string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
        PBXProject proj = new PBXProject();
        proj.ReadFromFile(projPath);
        string target = proj.TargetGuidByName("Unity-iPhone");
        Debug.Log("Enabling modules: CLANG_ENABLE_MODULES = YES");
        proj.AddBuildProperty(target, "CLANG_ENABLE_MODULES", "YES");
        proj.WriteToFile(projPath);
#endif
    }

    private static void DisableBitCode(BuildTarget buildTarget, string path)
    {
        #if UNITY_IOS
        if (buildTarget == BuildTarget.iOS)
        {
            Debug.Log("[UNITY_BUILD] Disable bitcode");

            string projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);

            string target = pbxProject.TargetGuidByName("Unity-iPhone");
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            pbxProject.WriteToFile(projectPath);
        }
#endif
    }

    private static void ProcessPostBuildPityTry(BuildTarget buildTarget, string path)
    {
        // Only perform these steps for iOS builds
#if UNITY_IOS

        Debug.Log("[UNITY_IOS] ProcessPostBuild - Adding Google Analytics frameworks.");

        // Go get pbxproj file
        string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

        // PBXProject class represents a project build settings file,
        // here is how to read that in.
        PBXProject proj = new PBXProject();
        proj.ReadFromFile(projPath);
        // This is the Xcode target in the generated project
        string target = proj.TargetGuidByName("Unity-iPhone");

        proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");
        proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-v");
        proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

        //if (!File.Exists(path + "/GoogleService-Info.plist"))
        //{
        //    FileUtil.CopyFileOrDirectory("Assets/GoogleService-Info.plist", path + "/GoogleService-Info.plist");
        //}
        //string guid = proj.AddFile("GoogleService-Info.plist", "GoogleService-Info.plist");
        //proj.AddFileToBuild(target, guid);

        // List of frameworks that will be added to project
        List<string> frameworks = new List<string>() {

            "AdSupport.framework",
            "AVFoundation.framework",
            "CoreGraphics.Framework",
            "CoreMedia.Framework",
            "CoreTelephony.Framework",
            "StoreKit.framework",
            "SystemConfiguration.framework",
            "UIKit.framework",
            "WebKit.framework",
            "libz.tbd",
            "iAd.framework",

            "AddressBook.framework",
            "AudioToolbox.Framework",
            "MessageUI.Framework",
            "SystemConfiguration.framework",
            "CoreData.framework"
    };

        // Add each by name
        frameworks.ForEach((framework) => {
            proj.AddFrameworkToProject(target, framework, false);
        });

        // List of frameworks that will be added to project
        List<string> usrLibFrameworks = new List<string>() {
        "libsqlite3.tbd",
        "libz.tbd",
        "libicucore.tbd",
    };

        // Add each by name
        usrLibFrameworks.ForEach((framework) => {
            proj.AddFileToBuild(target, proj.AddFile("usr/lib/" + framework, "Frameworks/" + framework, PBXSourceTree.Sdk));
        });


        // Write PBXProject object back to the file
        proj.WriteToFile(projPath);

#endif
    }
}

