﻿#if UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;

public class BuildPostProcess
{

    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);

            string target = pbxProject.TargetGuidByName("Unity-iPhone");
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            pbxProject.WriteToFile(projectPath);
        }
    }
}

public static class RemoveUnityAdsXcode
{
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget != BuildTarget.iOS)
            return;


        string[] filesToRemove = {
         "Classes/UnityAds/UnityAdsUnityWrapper.mm"
      };

        string pbxprojPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
        PBXProject proj = new PBXProject();
        proj.ReadFromString(File.ReadAllText(pbxprojPath));

        foreach (string name in filesToRemove)
        {
            string fileGuid = proj.FindFileGuidByProjectPath(name);
            if (fileGuid != null)
            {
                Debug.Log("Removing " + name + " from xcode project");
                proj.RemoveFile(fileGuid);
            }
        }

        File.WriteAllText(pbxprojPath, proj.WriteToString());
    }
}
#endif